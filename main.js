// BÀI 1
function bai1() {
  var i = 0;
  var sum = 0;
  for (i; sum <= 10000; i++) {
    sum += i;
    if (sum >= 10000) {
      break;
    }
  }
  document.getElementById(
    "ketQuaBai1"
  ).innerText = `Số nguyên dương nhỏ nhât: ${i} `;
}

// BÀI 2
function bai2() {
  var X = document.getElementById("coSo").value * 1;
  var n = document.getElementById("luyThua").value * 1;
  var sum = 0;

  for (var i = 1; i <= n; i++) {
    sum = sum + Math.pow(X, i);
  }
  document.getElementById("ketQuaBai2").innerText = `Tổng: ${sum} `;
}

// Bài 3
function bai3() {
  var giaiThua = document.getElementById("giaiThua").value;
  var i = 1;
  var tich = 1;
  for (i; i <= giaiThua; i++) {
    tich = tich * i;
  }
  document.getElementById("ketQuaBai3").innerText = `Giai thừa: ${tich} `;
}

// Bài 4
function bai4() {
  var soLe =
    "<div style='background-color: blue; color : white; padding: 0.5rem'>Div lẻ</div>";
  var soChan =
    "<div style='background-color: pink; color: white;padding: 0.5rem'>Div chẵn</div>";

  var result = "";
  var i = 1;
  for (i; i <= 10; i++) {
    if (i % 2 != 0) {
      result += soLe;
    } else if (i % 2 == 0) {
      result += soChan;
    }
  }
  document.getElementById("ketQuaBai4").innerHTML = result;
}

// Bài 5
function bai5() {
  var n = document.getElementById("nhapSoBai5").value;

  var daySoNguyenTo = "";
  for (var x = 2; x <= n; x++) {
    // var checkSNT = true;
    var checkSoNguyenTo = checkSNT(x);
    // for (var i = 2; i <= Math.sqrt(x); i++) {
    //   if (x % i == 0) {
    //     checkSNT = false;
    //     break;
    //   }
    // }
    if (checkSoNguyenTo) {
      daySoNguyenTo += x + ",";
    }
  }
  document.getElementById("ketQuaBai5").innerText = daySoNguyenTo;
}
function checkSNT(iSo) {
  var check = true;
  for (var i = 2; i <= Math.sqrt(iSo); i++) {
    if (iSo % i == 0) {
      check = false;
      break;
    }
  }
  return check;
}
